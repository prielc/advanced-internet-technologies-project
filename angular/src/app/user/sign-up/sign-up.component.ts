import { UserService } from './../user.service';
import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  @Output() addUser: EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserPs: EventEmitter<any> = new EventEmitter<any>();

  service:UserService;
  
  usrform = new FormGroup({
    username:new FormControl(),
    password:new FormControl(),
    name:new FormControl(),
    phone:new FormControl(),
  });
  
  sendData(){
    this.addUser.emit(this.usrform.value.username);
    console.log(this.usrform.value);
    this.service.postUser(this.usrform.value).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();         
      }
    );
    this.router.navigate(['/']);
  };

  constructor(service:UserService, private formBuilder:FormBuilder, private router: Router) {
    this.service=service;
  }

  ngOnInit() {
    this.usrform = this.formBuilder.group({
      username:  [null, [Validators.required]],
      password:[null, [Validators.required]],
      name:[null, [Validators.required]],
      phone:[null, [Validators.required]]
    });	          
  }

}
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-own-posts',
  templateUrl: './own-posts.component.html',
  styleUrls: ['./own-posts.component.css']
})
export class OwnPostsComponent implements OnInit {

  post;
  user;
  userkeys = [];

  constructor(private route:ActivatedRoute, private service:UserService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params =>{
      console.log(params);
      var id = params.get('id');
      this.service.getUser(id).subscribe(response =>{
        console.log(response)
        this.user = response.json();
        console.log(this.user.id)
        this.userkeys = Object.keys(this.user);
      })
    })
  }

}

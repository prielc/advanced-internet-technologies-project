import { UserService } from './user.service';
import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  
  token;
  user;
  userkeys = [];
  users;
  usersKeys = [];

  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();
  
  constructor(private route:ActivatedRoute, private service:UserService) {
  this.token = localStorage.getItem('token');
  }

  logout(){ 
    localStorage.removeItem('token');
  }

  optimisticAddPopu(users, usersKeys, id){
    for (let key of usersKeys){
      if(id==key){
        this.users[key].popularity = this.users[key].popularity + 1;
      }
    }
  }

  popularity(id){
    console.log(this.token);
    this.service.getPosts().subscribe(response=>{
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
      for (let key of this.usersKeys){
        if(key == id){
          console.log(this.users[id].popularity);
          this.service.putPopular(this.users[key].popularity + 1, id).subscribe(response => {
            console.log(response.json());
            this.updateUserPs.emit();
          });
          this.optimisticAddPopu(this.users, this.usersKeys, key);
        }
      }
    });
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params =>{
      console.log(params);
      console.log(this.token);
      if(this.token){
        var id = params.get('id');
        this.service.getUser(id).subscribe(response =>{
          console.log(response)
          this.user = response.json();
          console.log(this.user.id)
          this.userkeys = Object.keys(this.user);
        })
      }
    })
  }

}

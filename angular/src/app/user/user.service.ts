import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams} from '@angular/common/http';
import { Router } from "@angular/router";
import 'rxjs/Rx';
import { AngularFireDatabase } from 'angularfire2/database';
import {environment } from './../../environments/environment';

@Injectable()
export class UserService {

  http:Http;
  token;
  url = environment.url

  getUsers(){
    return this.http.get(environment.url + 'users/');
  }

  getUser(id){
    let options =  {
      headers:new Headers({
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
        'Token': this.token    
      })
    } 
    return this.http.get(environment.url + 'users/'+id,options);
  }

  getPost(id){
    let options =  {
      headers:new Headers({
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
        'Token': this.token    
      })
    } 
    return this.http.get(environment.url + 'posts/'+id,options);
  }

  postUser(data){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('username',data.username).append('password',data.password).append('name',data.name).append('phone',data.phone);
    return this.http.post(environment.url + 'users',params.toString(),options);
  }

  putPopular(data, id){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('popularity',data);
    return this.http.put(environment.url + 'users/'+ id ,params.toString(), options);
  }

  getPosts(){
    let options =  {
      headers:new Headers({
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
        'Token': this.token    
      })
    } 
    return this.http.get(environment.url + 'posts', options);
  }

  postPost(data){
    let options = {
      headers:new Headers({'content-type':'application/x-www-form-urlencoded'})
    }
    console.log(data.user_id);
    var params = new HttpParams().append('body',data.body).append('user_id',data.user_id);
    return this.http.post(environment.url + 'posts',params.toString(),options); 
  }

  putLikes(data, id){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('likes',data);
    return this.http.put(environment.url + 'posts/'+ id ,params.toString(), options);
  }

  login(credentials){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    var params = new HttpParams().append('username',credentials.username).append('password',credentials.password);
    return this.http.post(environment.url + 'login', params.toString(),options).map(response=>{ 
      let token = response.json().token;
      if (token){
        localStorage.setItem('token', token);
        console.log(token);
      }
    });
  }

  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
    this.token = localStorage.getItem('token');
  }

}

import { UserService } from './../user/user.service';
import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  
  invalid = false;
  token;
  posts;
  postskeys = [];
  postsKeys = [];
  postKeys = [];
  post;
  users;
  usersKeys = [];

  @Output() updatePost:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updatePostPs:EventEmitter<any> = new EventEmitter<any>();

  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();

  optimisticAdd(post){
    var newKey = this.postskeys[this.postskeys.length-1]+1;
    console.log(this.postskeys.length);
    console.log(this.postskeys);
    var newPostObject = {};
    newPostObject['body'] = post;
    console.log(newPostObject);
    this.posts[newKey] = newPostObject;
    this.postskeys = Object.keys(this.posts);
    console.log(this.posts);
    console.log(event,newKey);
  }

  pessimiaticAdd(){
    this.service.getPosts().subscribe(response => {
      this.posts =  response.json();
      this.postskeys = Object.keys(this.posts);
    });   
  }

  constructor(private service:UserService , private route: ActivatedRoute) {
    this.token = localStorage.getItem('token');
    if(this.token){
      this.service.getPosts().subscribe(response => {
        this.posts =  response.json();
        this.postskeys = Object.keys(this.posts);
      });
    }
  }

  logout(){ 
    localStorage.removeItem('token');
  }

  optimisticAddLike(posts, postsKeys, id){
    for (let key of postsKeys){
      if(id==key){
        this.posts[key].likes = this.posts[key].likes + 1;
      }
    }
  }

  likes(id){
    console.log(this.token);
    this.service.getPosts().subscribe(response=>{
      this.post = response.json();
      this.postKeys = Object.keys(this.post);
      for (let key of this.postKeys){
        if(key == id){
          console.log(this.post[id].likes);
          this.service.putLikes(this.post[key].likes + 1, id).subscribe(response => {
            console.log(response.json());
            this.updatePostPs.emit();
          });
          this.optimisticAddLike(this.post, this.postKeys, key);
        }
      }
    });
  }

  optimisticAddPopu(users, usersKeys, id){
    for (let key of usersKeys){
      if(id==key){
        this.users[key].popularity = this.users[key].popularity + 1;
      }
    }
  }

  popularity(id){
    console.log(this.token);
    this.service.getPosts().subscribe(response=>{
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
      for (let key of this.usersKeys){
        if(key == id){
          console.log(this.users[id].popularity);
          this.service.putPopular(this.users[key].popularity + 1, id).subscribe(response => {
            console.log(response.json());
            this.updateUserPs.emit();
          });
          this.optimisticAddPopu(this.users, this.usersKeys, key);
        }
      }
    });
  }

  ngOnInit() {
    console.log(this.token);
  }

}

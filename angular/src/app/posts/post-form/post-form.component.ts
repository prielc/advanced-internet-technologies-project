import { UserService } from './../../user/user.service';
import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  @Output() addPost: EventEmitter<any> = new EventEmitter<any>();
  @Output() addPostPs: EventEmitter<any> = new EventEmitter<any>();

  pstform = new FormGroup({
    body:new FormControl(),
    user_id:new FormControl(),
  });
  
  sendData(){
    this.addPost.emit(this.pstform.value);
    console.log(this.pstform.value);
    this.service.postPost(this.pstform.value).subscribe(
      response => {
        console.log(response.json());
        this.addPostPs.emit();         
      }
    );
    this.router.navigate(['/posts']);
  };


  constructor(private service:UserService, private formBuilder:FormBuilder, private router: Router, private route:ActivatedRoute) {
    this.service=service;
  }

  ngOnInit() {
    this.pstform = this.formBuilder.group({
      body:  [null, [Validators.required]]
    });	    
  }

}

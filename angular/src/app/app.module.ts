import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import { environment } from './../environments/environment';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { UserComponent } from './user/user.component';
import { PostsComponent } from './posts/posts.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { PostFormComponent } from './posts/post-form/post-form.component';
import { OwnPostsComponent } from './user/own-posts/own-posts.component';

import { PostsService } from './posts/posts.service';
import { UserService } from './user/user.service';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    UserComponent,
    PostsComponent,
    NavigationComponent,
    NotFoundComponent,
    AccessDeniedComponent,
    PostFormComponent,
    OwnPostsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'',component:LoginComponent},
      {path:'signup',component:SignUpComponent},
      {path:'users/:id',component:UserComponent},
      {path:'posts',component:PostsComponent},
      {path:'**', component:NotFoundComponent}
    ])
  ],
  providers: [
    PostsService,
    UserService,
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

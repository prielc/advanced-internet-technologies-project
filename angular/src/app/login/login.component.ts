import { UserService } from './../user/user.service';
import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup } from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalid = false;
  loginform = new FormGroup({
	  username:new FormControl(),
	  password:new FormControl(),	    
  });

  login(){
    this.service.login(this.loginform.value).subscribe(responser=>{
      this.router.navigate(['/posts']);
    },
    error=>{this.invalid= true;})
  }

  logout(){ 
    localStorage.removeItem('token');
    this.invalid= false; 
  }
  constructor(private service:UserService, private router:Router) { }

  ngOnInit() {
  	var value = localStorage.getItem('token');
  	
  	if (!value || value == undefined || value == "" || value.length == 0){		
  		this.router.navigate(['/']);  
  	}else{
      this.router.navigate(['/posts']);  		
  	}
  }

}
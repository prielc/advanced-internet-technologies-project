<?php

namespace Slim\Models;

class Post extends \Illuminate\Database\Eloquent\Model {

    public function post()
    {
    	return $this->belongsTo('Slim\Models\User');
    }

}
<?php
/*
namespace Slim\Middleware;
use Slim\Models\User;

class Auth {
    public function __invoke($request,$response,$next){
        $token      = $request->getHeaderLine('Token');                
        $path       = $request->getUri()->getPath();    
        $options    = $request->isOptions();            
        $_user      = User::where('token', '=', $token)->get();
            
        if($_user || $path == '/login' || $path == '/users/create' || $options){
        	$response = $next($request,$response);	
        	return $response->withHeader('Access-Control-Allow-Origin', '*')->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        }else{
        	return $response->withStatus(403)->withHeader('Access-Control-Allow-Origin', '*')->withHeader('Access-Control-Allow-Methods', '*');
        }
    }
}
<?php

require "bootstrap.php";

use Slim\Models\User;
use Slim\Models\Post;
use \Firebase\JWT\JWT;

$app = new \Slim\App();

//----------LOGIN----------
$app->post('/login', function($request, $response,$args){
    $username  = $request->getParsedBodyParam('username','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('username', '=', $username)->where('password', '=', $password)->get();
    if($_user[0]->id){
        $payload = ['token'=> $_user[0]->id];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});

//----------USERS----------
$app->get('/users', function($request, $response, $args){
    $_user = new User();
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'password'=>$usr->password,
            'name'=>$usr->name,
            'phone'=>$usr->phone,
            'popularity'=>$usr->popularity,
            'created_at'=>$usr->created_at,
            'updated_at'=>$usr->updated_at,
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $user = User::find($_id);
    $user->post;
    return $response->withStatus(200)->withJson($user);
});

$app->get('/posts/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $user = Post::find($_id);
    $user->post;
    return $response->withStatus(200)->withJson($user);
});

$app->post('/users', function($request, $response, $args){
    $username = $request->getParsedBodyParam('username','');
    $password = $request->getParsedBodyParam('password','');
    $name = $request->getParsedBodyParam('name','');
    $phone = $request->getParsedBodyParam('phone','');
    $popularity = $request->getParsedBodyParam('popularity','');
    $_user = new User();
    $_user->username = $username;
    $_user->password = $password;
    $_user->name = $name;
    $_user->phone = $phone;
    $_user->popularity = $popularity;
    $_user->save();
    if($_user->id){
         $payload = ['user id: '=>$_user->id];
         return $response->withStatus(201)->withJson($payload);
     }
     else{
         return $response->withStatus(400);
     }
  });

$app->put('/users/{id}', function($request, $response, $args){
    $popularity = $request->getParsedBodyParam('popularity','');
    $_user = User::find($args['id']);
    $_user->popularity = $popularity;
    if($_user->save()){
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//----------POSTS----------
$app->get('/posts', function($request, $response, $args){
    $_post = new post();
    $posts = $_post->all();
    $payload = [];
    foreach($posts as $pst){
        $payload[$pst->id] = [
            'id'=>$pst->id,
            'body'=>$pst->body,
            'user_id'=>$pst->user_id,
            'likes'=>$pst->likes
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->post('/posts', function($request, $response, $args){
    $body = $request->getParsedBodyParam('body','');
    $user_id = $request->getParsedBodyParam('user_id','');
    $_post = new Post();
    $_post->body = $body;
    $_post->user_id = $user_id;
    $_post->save();
    if($_post->id){
        $payload = ['post id: '=>$_post->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->put('/posts/{id}', function($request, $response, $args){
    $likes = $request->getParsedBodyParam('likes','');
    $_post = Post::find($args['id']);
    $_post->likes = $likes;
    if($_post->save()){
        $payload = ['post_id' => $_post->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//----------Data Security----------
/*$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});*/

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, Token, x-www-form-urlencoded')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();